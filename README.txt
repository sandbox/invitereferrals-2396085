INTRODUCTION
------------
InviteReferrals : Simplest Referral Marketing Software to design 
and launch Customer Referral Campaigns within minutes to reach 
new customers. Install once, edit or create multiple campaigns 
without changing the code ever on your site. 
http://www.invitereferrals.com
      
REQUIREMENTS
------------
No special requirements

INSTALLATION
------------
* Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
1. Visit your invitereferrals option 'Configure' to configure the settings.
2. Get Secret Key and brandid as explained in the following steps :
-Go to www.invitereferrals.com and login (using information from registration)
-Go to the Doc/Integration section on sidebar
-Note the BrandID and Secret Key
